/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <fcntl.h>
#include <locale.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <wchar.h>

#include <ncurses.h>

#define UNUSED(x) (void)(x)

typedef union {
    ssize_t s;
    int i;
} Arg;

typedef struct {
    short fg;
    short bg;
} Color;

typedef struct {
    char **items;
    size_t sel;
    size_t off;
    size_t count;
} Items;

typedef struct {
    int key;
    void (*fn)(const Arg *arg);
    Arg arg;
} Key;

typedef struct {
    char *str;
    int len;
    int pos;
} Query;

static void cleanup(void);
static void delete(const Arg *arg);
static void insert(const Arg *arg);
static void redraw(void);
static int match(const char *item, const char *query);
static size_t mbslen(const char *s, size_t n);
static ssize_t mbsnext(const char *s, size_t pos, size_t n);
static ssize_t mbsprev(const char *s, size_t pos, size_t n);
static void movecursor(const Arg *arg);
static ssize_t nextitem(size_t idx, char direction);
static void selectitem(const Arg *arg);
static void quit(const Arg *arg);

#include "config.h"

static Items items;
static Query query;
static FILE *tty;
static SCREEN *screen;

static int opt_exitnochoice;
static int opt_reverse;
static int opt_selectone;

static void updatesel(void)
{
    ssize_t idx;

    if (!items.count)
        return;

    if (!match(items.items[items.sel], query.str) &&
            ((idx = nextitem(items.sel, 1)) != -1 ||
            (idx = nextitem(items.sel, -1)) != -1))
        items.sel = idx;
}

static void cleanup(void)
{
    size_t i;

    for (i = 0; i < items.count; i++)
        free(items.items[i]);
    free(items.items);
    free(query.str);

    fclose(tty);

    if (!isendwin())
        endwin();
    delscreen(screen);
}

static void delete(const Arg *arg)
{
    size_t width;
    ssize_t pos;

    if ((arg->i <= 0 && !query.pos) || (arg->i > 0 && query.pos == query.len))
        return;

    if (arg->i <= 0 && (pos = mbsprev(query.str, query.pos, query.len)) == -1)
        return;
    else if (arg->i > 0 && (pos = mbsnext(query.str, query.pos, query.len)) == -1)
        return;

    width = mblen(query.str + pos, query.len);

    if (query.len != query.pos)
        memmove(query.str + pos,
            query.str + pos + width, query.len - pos + width);

    query.len -= width;
    query.pos -= width;
    query.str[query.len] = '\0';

    updatesel();
}

static void insert(const Arg *arg)
{
    query.str = realloc(query.str, query.len + 2);

    if (query.len != query.pos)
        memmove(query.str + query.pos + 1,
            query.str + query.pos, query.len - query.pos + 1);

    query.str[query.pos++] = (char) arg->i;
    query.str[++query.len] = '\0';

    updatesel();
}

static void die(const char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    puts("");

    exit(1);
}

static void handle_winch(int signal)
{
    UNUSED(signal);

    endwin();
    refresh();
    redraw();
}

static int match(const char *item, const char *query)
{
    if (query == NULL)
        return 1;

    for (; *item != '\0' && *query != '\0'; item++) {
        if (*item == *query)
            query++;
    }

    return *query == '\0';
}

static size_t mbslen(const char *s, size_t n)
{
    size_t total = 0;
    while (n) {
        size_t len = mblen(s, n);
        n -= len;
        s += len;
        total++;
    }
    return total;
}

static ssize_t mbsnext(const char *s, size_t pos, size_t n)
{
    int width;
    if ((width = mblen(s + pos, n - pos)) != -1)
        return pos + width;
    return -1;
}

static ssize_t mbsprev(const char *s, size_t pos, size_t n)
{
    while (pos--)
        if (mblen(s + pos, n) != -1)
            return pos;
    return -1;
}

static ssize_t nextitem(size_t idx, char direction)
{
    if (opt_reverse)
        direction *= -1;

    while (1) {
        /* Avoid overflows */
        if (idx == 0 && direction < 0)
            return -1;
        if ((idx == items.count - 1) && direction > 0)
            return -1;

        idx += direction;

        if (match(items.items[idx], query.str))
            return idx;
    }
}

static void movecursor(const Arg *arg)
{
    int pos;

    if (arg->i <= 0)
        pos = mbsprev(query.str, query.pos, query.len);
    else
        pos = mbsnext(query.str, query.pos, query.len);

    if (pos < 0 || pos > query.len)
        return;

    query.pos = pos;
}

static void selectitem(const Arg *arg)
{
    int rows, direction = arg->s < 0 ? -1 : 1;
    ssize_t idx, counter;

    for (idx = items.sel, counter = arg->s; counter; counter -= direction)
        if ((idx = nextitem(idx, direction)) == -1)
            return;

    items.sel = idx;

    rows = getmaxy(stdscr) - 2;

    /* Scroll up/down if necessary */
    if ((items.sel - items.off) >= (size_t) rows)
        items.off += arg->s * (opt_reverse ? -1 : 1);
    else if (rows - (ssize_t) (items.sel - items.off) < 0)
        items.off += arg->s * (opt_reverse ? -1 : 1);
}

static void getlines(void)
{
    char **lines = NULL, *line = NULL;
    size_t n = 0, len = 0;

    while (getdelim(&line, &len, '\n', stdin) != -1) {
        const char *ptr = line;

        while (isspace(*ptr))
            ptr++;

        if (*ptr != '\0') {
            lines = realloc(lines, ++n * sizeof(char *));
            lines[n - 1] = line;
        } else {
            free(line);
        }

        line = NULL;
        len = 0;
    }

    free(line);

    items.items = lines;
    items.sel = 0;
    items.count = n;
    items.off = 0;
}

static void printitem(int row, size_t idx)
{
    int rows = getmaxy(stdscr) - 2;

    if (opt_reverse)
        row = rows - row - 1;

    if (idx == items.sel)
        color_set(COLOR_SELECTION, NULL);

    mvprintw(row, 0, "%s", items.items[idx]);

    if (idx == items.sel)
        color_set(COLOR_DEFAULT, NULL);
}

static void redraw(void)
{
    size_t i, max_x, max_y, y, selected;

    getmaxyx(stdscr, max_y, max_x);

    clear();

    for (y = 0, i = items.off; i < items.count && y < max_y - 2; i++)
        if (match(items.items[i], query.str))
            printitem(y++, i);

    if (y == 0 || !items.count)
        selected = 0;
    else
        selected = items.sel + 1;

    color_set(COLOR_STATUS, NULL);
    for (i = 0; i < max_x; i++)
        mvaddch(max_y - 2, i, ' ');
    mvprintw(max_y - 2, 0, "%zu/%zu", selected, items.count);
    color_set(COLOR_DEFAULT, NULL);

    if (query.len)
        mvprintw(max_y - 1, 0, "%s", query.str);

    move(max_y - 1, mbslen(query.str, query.pos));
    refresh();
}

static void run(void)
{
    Arg arg;
    int ret;
    wint_t c;
    unsigned i;

    if (opt_exitnochoice && items.count == 0)
        return;
    if (opt_selectone && items.count == 1) {
        arg.s = 1;
        quit(&arg);
    }

    updatesel();
    redraw();

    while ((ret = get_wch(&c)) != ERR) {
        for (i = 0; i < (sizeof(keys) / sizeof(*keys)); i++) {
            if (keys[i].key == (wchar_t) c) {
                keys[i].fn(&keys[i].arg);
                goto loop;
            }
        }

        if (ret != KEY_CODE_YES) {
            wchar_t wcs[2] = { c, '\0' };
            char *mbs;
            size_t len;

            len = wcstombs(NULL, wcs, 0);
            mbs = malloc(len + 1);
            len = wcstombs(mbs, wcs, len + 1);

            for (i = 0; i < len; i++) {
                arg.i = mbs[i];
                insert(&arg);
            }

            free(mbs);
        }

loop:
        redraw();
    }
}

static void setup(void)
{
    unsigned i;

    if (setlocale(LC_CTYPE, "") == NULL)
        die("Could not set locale");

    if (isatty(fileno(stdin)))
        die("Can only read input from pipes");

    if ((tty = fopen("/dev/tty", "r+")) == NULL)
        die("Could not open TTY");

    if (signal(SIGWINCH, handle_winch) == SIG_ERR)
        die("Could not set up signal handling");

    if ((screen = newterm(NULL, tty, tty)) == NULL)
        die("Failed to create terminal");

    set_term(screen);

    if (cbreak() == ERR ||
            noecho() == ERR ||
            keypad(stdscr, TRUE) == ERR)
        die("Could  not set up ncurses");

    atexit(cleanup);

    if (start_color() == ERR ||
            use_default_colors() == ERR)
        die("Could not set up colors");

    for (i = 0; i < COLOR_MAX; i++)
        if (init_pair(i, colors[i].fg, colors[i].bg) == ERR)
            die("Could not initialize color pairs");
}

static void quit(const Arg *arg)
{
    char selected = arg && arg->i;

    endwin();
    if (selected)
        fprintf(stdout, "%s", items.items[items.sel]);

    exit(selected ? 0 : 1);
}

static void usage(const char *program, char error)
{
    FILE *f = error ? stderr : tty ? tty : stdout;

    fprintf(f,
            "USAGE: %s [<OPTS>...]\n"
            "\n"
            "  -h           Show this help\n"
            "  -q <STR>     Start with the given query\n"
            "  -r           Reverse ordering of lines\n"
            "  -0           Immediately exit if there are no choices\n"
            "  -1           Immediately print item if only one choice exists\n"
            , program);
}

int main(int argc, char *argv[])
{
    int opt;

    while ((opt = getopt(argc, argv, "hq:r01")) != -1) {
        switch (opt) {
            case 'h':
                usage(argv[0], 0);
                return 0;
            case 'q':
                query.str = strdup(optarg);
                query.pos = query.len = strlen(optarg);
                break;
            case 'r':
                opt_reverse = 1;
                break;
            case '0':
                opt_exitnochoice = 1;
                break;
            case '1':
                opt_selectone = 1;
                break;
            default:
                usage(argv[0], 1);
                return 1;
        }
    }

    setup();
    getlines();
    run();

    return 0;
}
