static Key keys[] = {
    { 27,            quit,        { .i =  0 } },
    { '\n',          quit,        { .i =  1 } },
    { KEY_ENTER,     quit,        { .i =  1 } },
    { KEY_BACKSPACE, delete,      { .i = -1 } },
    { 127,           delete,      { .i = -1 } },
    { '\b',          delete,      { .i = -1 } },
    { KEY_DC,        delete,      { .i = +1 } },
    { KEY_UP,        selectitem,  { .s = -1 } },
    { KEY_DOWN,      selectitem,  { .s = +1 } },
    { KEY_LEFT,      movecursor,  { .i = -1 } },
    { KEY_RIGHT,     movecursor,  { .i = +1 } },
};

enum {
    COLOR_DEFAULT,
    COLOR_SELECTION,
    COLOR_STATUS,
    COLOR_MAX
};

static Color colors[COLOR_MAX] = {
    { -1,           -1          }, /* default color */
    { COLOR_YELLOW, -1          }, /* selection color */
    { -1,           COLOR_BLACK }, /* status color */
};
